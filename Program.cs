﻿using System;
using TestDotNetBibliotheque.Entities;
namespace TestDotNetBibliotheque
{
    class Program
    {
        private static Store store;

        static void Main(string[] args)
        {

            Init();

            while(true)
            {
                MenuUI();
            }

        }

        private static void Init()
        {
            store = new Store();
            Console.WriteLine("Hello World!\n");
            Console.WriteLine("Library JSON schema File Name : \n");
            string filename = Console.ReadLine();
            store.Import(filename);
            Console.WriteLine("\nImporting Data !");
            Console.Clear();
        }

        private static void MenuUI()
        {
            Console.WriteLine("Library Menu : \n");
            Console.WriteLine("1- Get Quantity By Book Name \n");
            Console.WriteLine("2- Buy Multiple Books \n");
            Console.WriteLine("3- Exit \n");
            Console.WriteLine("\nChoice number : ");

            char input = Console.ReadKey().KeyChar;

            Console.WriteLine("\n");

            switch (input)
            {
                case '1' :
                    Console.WriteLine("1- Get Quantity By Book Name \n");
                    Console.WriteLine("Tape book's name : \n");
                    string name = Console.ReadLine();
                    int result = store.Quantity(name);
                    Console.WriteLine($"Quantity = {result}\n");
                    Console.WriteLine("Press Space to continue");
                    Console.ReadKey();
                    Console.WriteLine("\n");
                    Console.Clear();
                    break;

                case '2' :
                    Console.WriteLine("2- Buy Multiple Books \n");
                    Console.WriteLine("Tape book's names : \n");
                    string inputNames = Console.ReadLine();
                    string[] names = inputNames.Split(',');
                    double results = store.Buy(names);
                    Console.WriteLine($"Total Price = {results}\n");
                    Console.WriteLine("Press Space to continue");
                    Console.ReadKey();
                    Console.WriteLine("\n");
                    Console.Clear();
                    break;

                case '3' :
                    Environment.Exit(0);
                    break;

                default:
                    break;
            }


        }

    }
}
