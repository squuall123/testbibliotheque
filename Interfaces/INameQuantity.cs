﻿using System;
namespace TestDotNetBibliotheque.Interfaces
{
    public interface INameQuantity
    {
        string Name { get; }
        int Quantity { get; }
    }
}
