﻿using System;
using System.Collections.Generic;
using TestDotNetBibliotheque.Interfaces;

namespace TestDotNetBibliotheque.Exceptions
{
    public class NotEnoughInventoryException : Exception
    {
        public IEnumerable<INameQuantity> Missing { get; }
    }
}
