﻿using System;
using TestDotNetBibliotheque.Interfaces;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using TestDotNetBibliotheque.Exceptions;

namespace TestDotNetBibliotheque.Entities
{
    public class Store : IStore
    {
        private JArray categories;
        private JArray catalogs;
        private JObject jsonFile;
        private List<NameQuantity> booksToBuy;

        public void Import(string catalogAsJson)
        {
            try
            {
                jsonFile = JObject.Parse(File.ReadAllText(@"Schemas/" + catalogAsJson + ".json"));
            }
            catch
            {
                throw new FileNotFoundException();
            }


            if(jsonFile.ContainsKey("Category") && jsonFile.ContainsKey("Catalog"))
            {
                categories = (JArray)jsonFile["Category"];
                catalogs = (JArray)jsonFile["Catalog"];

            } else {
                Console.WriteLine("Bad JSON format !!");
            }
          
        }

        public int Quantity(string name)
        {
            try
            {
                return catalogs.Where(livre => livre.Value<string>("Name").Equals(name)).Select(livre => livre.Value<int>("Quantity")).SingleOrDefault();

            }
            catch 
            {
                throw new InvalidOperationException();
            }
        }

        public double Buy(params string[] basketByNames)
        {

            var missing = new List<NameQuantity>();

            var books = basketByNames.GroupBy(book => book).Select(arg => new NameQuantity(arg.Key, arg.Count()));

            foreach (var item in books)
            {
                var checkExist = catalogs.FirstOrDefault(book => book.Value<string>("Name") == item.Name);

                if (checkExist == null)
                {
                    missing.Add(new NameQuantity(
                        item.Name,
                        item.Quantity
                        ));
                }
                else
                {
                    int maxQuantity = checkExist.Value<int>("Quantity");

                    if (item.Quantity > maxQuantity)
                    {
                        missing.Add(new NameQuantity(
                            item.Name,
                            item.Quantity - maxQuantity));
                    }
                }
            }

            if (missing.Count > 0)
            {
                throw new NotEnoughInventoryException();

            }
            else
            {

                double price = 0;

                foreach (var book in books)
                {
                    var element = catalogs.First(livre => livre.Value<string>("Name") == book.Name);
                    price += Math.Round(book.Quantity * element.Value<double>("Price"), 2);

                }

                var booksCategories = new Dictionary<string, string>();

                foreach (var book in books)
                {
                    var bk = catalogs.First(arg => arg.Value<string>("Name") == book.Name);
                    string category = bk.Value<string>("Category");
                    booksCategories.Add(book.Name, category);
                }

                var categoriesQuantities = from book in books
                                           join bookCategory in booksCategories on book.Name equals bookCategory.Key
                                           select new { Category = bookCategory.Value, book.Quantity };

                var categoriesDiscount = categoriesQuantities.GroupBy(
                    b => b.Category,
                    b => b.Quantity,
                    (category, quantity) => new
                    {
                        Category = category,
                        Sum = quantity.Sum()
                    })
                    .Where(g => g.Sum > 1)
                    .Select(g => g.Category)
                    .ToList();

                double amount = 0;

                foreach (var discount in booksCategories.Where(b => categoriesDiscount.Contains(b.Value)))
                {
                    var book = catalogs.First(arg => arg.Value<string>("Name").Equals(discount.Key));
                    var category = categories.First(arg => arg.Value<string>("Name").Equals(discount.Value));

                    double unitaryPrice = book.Value<double>("Price");
                    double discountValue = category.Value<double>("Discount");
                    amount += Math.Round(unitaryPrice * discountValue, 2);
                }




                return price - amount;

            }
        }

    }
}

