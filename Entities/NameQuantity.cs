﻿using System;
using TestDotNetBibliotheque.Interfaces;

namespace TestDotNetBibliotheque.Entities
{
    public class NameQuantity : INameQuantity
    {

        public NameQuantity(string name, int quantity) 
        {
            this.Name = name;
            this.Quantity = quantity;
        }
        public string Name { get; }
        public int Quantity { get; }

    }
}
